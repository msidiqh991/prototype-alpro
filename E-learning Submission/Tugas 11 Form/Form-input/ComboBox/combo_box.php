<!DOCTYPE html>
<html lang="en">
<head>
    <title>Example Combo Box</title>
</head>
<body>
    <form action="prosescb-box.php" method="post">
        <h2>Pillih Film Terfavorit</h2>
        <select name="film">
            <option value="0" disabled selected> - </option>
            <option value="TA">The Avengers</option>
            <option value="TW">The Walking Dead</option>
            <option value="HG">Hunger Games</option>
            <option value="MV">Music Video</option>
            <option value="DCS">DC Series</option>
        </select>
        <br><br>
        <input type="submit" value="Submit" name="Pilih">
    </form>
</body>
</html>