<?php
    echo "Baca Array dengan FOR <br>";
    echo "- <br>";
    $listteman = array("Sidiq","Adit","Syihab","Suryo","Renbo");
    for($i=0; $i<6; $i++){
        echo "$listteman[$i]";
        echo "<br>";
    }

    echo "Baca Array dengan foreach <br>";
    foreach($listteman as $val){
        echo "$val <br>";
    }
?>