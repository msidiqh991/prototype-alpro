<?php 
    $nilai = 73.00;
    echo "Keterangan Nilai : $nilai <br>";

    if($nilai >= 80.00 && $nilai < 100){
        echo "Mendapatkan Grade A, dengan Numerik 4.00";
    } else if($nilai >= 76.25 && $nilai < 79.99){
        echo "Mendapatkan Grade A-, dengan Numerik 3.67";
    } else if($nilai >= 68.75 && $nilai < 76.24){
        echo "Mendapatkan Grade B+, dengan Numerik 3.33";
    } else if($nilai >= 65.00 && $nilai < 68.47){
        echo "Mendapatkan Grade B, dengan Numerik 3.00";
    } else if($nilai >= 62.50 && $nilai < 64.99){
        echo "Mendapatkan Grade B-, dengan Numerik 2.67";
    } else if($nilai >= 57.50 && $nilai < 62.49){
        echo "Mendapatkan Grade C+, dengan Numerik 2.33";
    } else if($nilai >= 55.00 && $nilai < 57.49){
        echo "Mendapatkan Grade C, dengan Numerik 2.00";
    } else if($nilai >= 51.25 && $nilai < 54.99){
        echo "Mendapatkan Grade C-, dengan Numerik 1.67";
    } else if($nilai >= 43.75 && $nilai < 51.24){
        echo "Mendapatkan Grade D+, dengan Numerik 1.33";
    } else if($nilai >= 40.00 && $nilai < 43.74){
        echo "Mendapatkan Grade D, dengan Numerik 1.00";
    } else if($nilai >= 0.00 && $nilai < 39.99){
        echo "Mendapatkan Grade E, dengan Numerik 0.00";
    } else {
        echo "Data invalid";
    }
?>