<!DOCTYPE html>
<html lang="en">
<head>
    <title>Validation Form</title>
    <link rel="stylesheet" href="../css/alter.css">
</head>
<body>
    <style>
        .error{
            color: red;
        }
    </style>
    <?php 
    $nameErr = $emailErr = $genderErr = $websiteErr = "";
    $name = $email = $gender = $comment = $website = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){ 
        if(empty($_POST['name'])){
            $nameErr = "Name is required";
        } else {
            $name = test_input($_POST["name"]);
            if(!preg_match("/^[a-zA-Z ]*$/", $name)){
                $nameErr = "Only letters and white space allowed";
            }
        }
        if(empty($_POST["email"])){
            $emailErr = "Email is required";
        } else {
            $email = test_input($_POST["email"]);
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $emailErr = "Invalid Email Format";
            }
        }
        if(empty($_POST["website"])){
            $website = "";
        } else {
            $website = test_input($_POST["website"]);
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%?=~_|]/i", $website)) {
                $websiteErr = "Invalid URL";
            }
        }
        if (empty($_POST["comment"])) {
            $comment = "";
        } else {
            $comment = test_input($_POST["comment"]);
        }
        if (empty($_POST["gender"])) {
            $genderErr = "Gender harus diisi";
        } else {
            $gender = test_input($_POST["gender"]);
        }
    }

    function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>
    <div class="container">
    <h3>Laman Validasi Form</h3>
    <p>
        <span class="error">* harus diisi</span>
    </p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" name="">
        Name: 
        <input type="text" name="name" value="<?php echo $name; ?>">
        <span class="error">
            * <?php echo $nameErr; ?>
        </span>
        <br><br>
        Email: 
        <input type="text" name="email" value="<?php echo $email; ?>">
        <span class="error">
            * <?php echo $emailErr; ?>
        </span>
        <br><br>
        Website: <input type="text" name="website" value="<?php echo $website; ?>">
        <span class="error">
            <?php echo $websiteErr; ?>
        </span>
        <br><br>
        Comment: <textarea name="comment" id="" cols="40" rows="5" value="<?php echo $comment; ?>"></textarea>
        <br><br>
        Gender: 
        <span class="error">
            * <?php echo $genderErr; ?>
        </span>
        <br>
        <input type="radio" name="gender" <?php
            if(isset($gender) && $gender == "female") 
            echo "checked"; ?> value="female"> Female
        <br>
        <input type="radio" name="gender" <?php
            if(isset($gender) && $gender == "male") 
            echo "checked"; ?> value="male"> Male
        <br><br>
        <input type="submit" class="btn-green" name="submit" value="Submit">
    </form>
    <br>
    <hr>
    <?php
        echo "<h3><u>Hasil Output</u></h3>";
        echo $name;
        echo "<br>";
        echo $email;
        echo "<br>";
        echo $website;
        echo "<br>";
        echo $comment;
        echo "<br>";
        echo $gender;
    ?>
    </div>
</body>
</html>