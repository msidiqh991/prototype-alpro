<?php 
    $arrNilai = array("Fulan"=>80, "Fulin"=>90, "Fulun"=>5, "Falan"=>85);
    echo "<b>Array sebelum diurutkan</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    sort($arrNilai);
    ksort($arrNilai);
    echo "<b>Array setelah diurutkan dengan ksort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    krsort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan krsort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
?>